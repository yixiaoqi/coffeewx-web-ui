import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/wx/account/fans/list',
    method: 'post',
    params:data
  })
}

export function createRow(data) {
  return request({
    url: '/api/wx/account/fans/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/wx/account/fans/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/wx/account/fans/delete',
    method: 'post',
    params:data
  })
}

export function syncAccountFans(data) {
  return request({
    url: '/api/wx/account/fans/syncAccountFans',
    method: 'post',
    data
  })
}

export function sendMsg(data) {
  return request({
    url: '/api/wx/account/fans/sendMsg',
    method: 'post',
    data
  })
}
