/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const sysUserManage = {
  path: '/sysUserManage',
  component: Layout,
  alwaysShow: true,
  name: '系统管理',
  redirect: 'noredirect',
  meta: {
    title: '系统管理',
    icon: 'component'
  },
  children: [
    {
      path: 'userList',
      component: () => import('@/views/system-web/system-manage/user-manage/userList'),
      name: '用户管理',
      meta: { title: '用户管理', icon: 'user', noCache: true }
    }
  ]
}

export default sysUserManage
